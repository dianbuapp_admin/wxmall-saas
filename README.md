# jfinal-wxmall-saas

#### 项目介绍
wxmall saas版本

基于Jfinal Jboot微服务开发的saas版本商城系统
项目包含微信公众号h5客户端，小程序客户端
商户后台管理系统

支持对接多个公众号，多个小程序；
支持扫码授权微信小程序，微信公众号
支持提交微信第三方服务平台验证覆盖全网服务

#### 软件架构

- JDK8 + 
- 基于JFinal核心框架开发
- 基于JBoot微服务开发

商户后台截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105358_af77b322_471938.png "TIM图片20190215104635.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105417_7ec3d355_471938.png "TIM图片20190215104725.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105425_fc2cd0db_471938.png "TIM图片20190215104744.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105435_c2c2bda7_471938.png "TIM图片20190215104813.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105448_cdd4ac47_471938.png "TIM图片20190215104837.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105459_e8d422d9_471938.png "TIM图片20190215104921.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105512_05728043_471938.png "TIM图片20190215105042.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105524_4386e536_471938.png "TIM图片20190215105100.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105534_668b91d4_471938.png "TIM图片20190215105118.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105544_10a7de9e_471938.png "TIM图片20190215105133.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105554_dc898cae_471938.png "TIM图片20190215105158.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105601_adbf8a2d_471938.png "TIM图片20190215105216.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105613_c772f45a_471938.png "TIM图片20190215105253.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/105620_1964c7c1_471938.png "TIM图片20190215105316.png")

小程序端截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/101303_260b307f_471938.png "TIM图片20190216100719.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/101311_bc26308f_471938.png "TIM图片20190216100915.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/101319_512eb016_471938.png "TIM图片20190216100937.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/101331_6f9d4d3f_471938.png "TIM图片20190216101118.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/101337_e7fa7a38_471938.png "TIM图片20190216101147.png")

代码结构说明
![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/164420_daa0fb9a_471938.png "TIM图片20180823164410.png")

1. weapp-model工程
    提供数据模型，由系统生成，不可更改其代码，在model模块中找到ModelGenApp类，运行其main方法，生成model实体
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/164714_b00dc475_471938.png "TIM图片20180823164706.png")
2. weapp-service-api工程
    接口抽象工程
    基础接口由系统生成，每个数据表对应一个service基础类，提供model的增删改查基本方法
    在service工程中找到ServiceCodeGenApp类，运行其main方法，生成基础接口类，生成后，可以修改，自定义其他业务逻辑接口方法
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/165306_905be8b6_471938.png "TIM图片20180823165142.png")
3. weapp-service-provider工程
    接口实现工程，微服务，服务提供者
    同样可通过provider工程的gen目录下的ServiceImplCodeGenApp类来生成基础接口实现类，运行其main方法即可
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/170250_decfc3c7_471938.png "TIM图片20180823170242.png")
4. weapp-sched
    weapp分布式调度框架，最简洁的调度实现代码
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/170428_8229c133_471938.png "TIM图片20180823170420.png")
5. weapp-web-core
    所有web工程依赖的核心工程，提供web基础类封装
6. weapp-web-admin
    商户管理后台工程
7. weapp-web-plat
    平台运维管理后台工程
8. weapp-web-mobile
    微信公众号客户端工程
9. weapp-web-api
    小程序接口工程
10. weapp-wxsdk-open工程
    微信接口api封装工程
11. weapp_template
    weapp小程序客户端工程
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/171444_098630ce_471938.png "TIM图片20180823171435.png")

#### 安装部署

1. wxmall是maven管理项目，获取源码后，导入到eclipse中
2. 导入数据库
3. 首先运行provider工程
    provider工程直接找到工程下面的StartApp类，运行其main方法启动应用，使用内置服务器undertow
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172159_0ddc4f6d_471938.png "TIM图片20180823172151.png")
4. 然后再运行web工程
5. web工程推荐使用jetty-run插件运行，只支持jetty9以上的服务器
    eclipse中安装jetty插件
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/171931_b1065c22_471938.png "TIM图片20180823171924.png")
6. 每个web工程都做如下操作
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172339_5a182fce_471938.png "TIM图片20180823172331.png")

7. 打包部署
    使用maven 命令     clean package appassembler:generate-daemons
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172452_00cf049e_471938.png "TIM图片20180823172437.png")

#### 配置说明

1. weapp-service-api工程中的公共配置说明如下
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172701_fd3e803e_471938.png "TIM图片20180823172654.png")
2. 数据库配置如下
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/0823/172836_cdb0be0c_471938.png "TIM图片20180823172829.png")
3. jboot相关分布式部署配置请参考jboot文档

#### 使用说明

1. 本项目是收费项目
2. 付费后获取最新最全源码
3. 代码最终解析权属于点步科技

合作请加作者微信

![输入图片说明](https://images.gitee.com/uploads/images/2019/0215/140151_888c0a4f_471938.jpeg "微信图片_20190215135628.jpg")